Some time ago my family and I visited Taranaki for an easter break, and while
we were there we passed through the little township of Oakura.

I was quite taken with the signwriting on the Oakura Hall, and took a photo of
it while we stopped there briefly.  After the trip, we got home again I decided
to learn how to create a font, and selected this as my template to work from.

This is the result.  Created some time ago now (as I write this in 2018 the
last commit is from 2012) I used the font for a few things, and submitted it
to a "Digital Mashups" competition that was on, and pretty much forgot all
about it.

Roll on 2018, and I went hunting for it on the internet only to discover the
internet just isn't as good a backup mechanism as I thought!  I'd uploaded
to Gitorious - one of the free Git hosting sites at the time, but they went
out of business in the interim and are no longer on the internet.

So here it is once more, on GitLab this time (who I think might have even
purchased Gitorious, but who clearly didn't transition the projects across).

Enjoy!

![the photo of Oakura Hall that started it all](//gitlab.com/karora/oakura/raw/bf2d291bedc46d13188349abe239dd56cec713dd/_dsc5744-02.jpg)
